# TYPO3 Extension `redirect2pagetypesuffix`

This extension redirects urls without the configured page type suffix to the one with, e.g., `.html` at the end. This avoids duplicate content issues.

## Requirements

* TYPO3 9 LTS or 10 LTS

## Installation and Setup
Install this extension, either by downloading from the TER or by using composer with `composer require hziegenhain/redirect2pagetypesuffix`.

### Optional Configuration

You can configure the status code used for the redirect. By default it is set to *307*.


---


_Originally made as studiomitte/redirect2trailingslash by Georg Ringer from [StudioMitte](https://studiomitte.com) with ♥_
