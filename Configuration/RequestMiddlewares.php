<?php

$rearrangedMiddlewares = true;

return [
    'frontend' => [
        'typo3/redirect2pagetypesuffix/redirect' => [
            'target' => \Hziegenhain\Redirect2pagetypesuffix\Http\PageTypeSuffixRedirect::class,
            'before' => [
                'typo3/cms-frontend/page-resolver',
            ],
            'after' => [
                'typo3/cms-frontend/static-route-resolver'
            ],
        ]
    ],
];
