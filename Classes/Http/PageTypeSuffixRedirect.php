<?php

declare(strict_types=1);

/**
 * This file is part of the "redirect2pagetypesuffix" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Hziegenhain\Redirect2pagetypesuffix\Http;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Http\RedirectResponse;
use TYPO3\CMS\Core\Site\Entity\NullSite;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class PageTypeSuffixRedirect implements MiddlewareInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        /** @var Site $site */
        $site = $request->getAttribute('site', null);

        if ($site && !$site instanceof NullSite) {
            $defaultPageTypeSuffix = $site->getConfiguration()['routeEnhancers']['PageTypeSuffix']['default'];
            $path = $request->getUri()->getPath();
            $pathInfo = pathinfo($path);

            if (!isset($pathInfo['extension']) && $path !== '/' && $defaultPageTypeSuffix !== '/') {
                $path = rtrim($path, "/");
                $path .= $defaultPageTypeSuffix;
                $redirectUri = $request->getUri()->withPath($path);
                return new RedirectResponse(
                    $redirectUri,
                    $this->getStatusCode(),
                    [
                        'X-Redirect-By' => 'TYPO3 Redirect2Pagetypesuffix Redirect'
                    ]
                );
            }
        }

        return $handler->handle($request);
    }

    private function getStatusCode(): int
    {
        $statusCode = 0;
        try {
            $statusCode = (int)GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('redirect2pagetypesuffix', 'statusCode');
        } catch (\Exception $e) {
            // do nothing
        }
        if (!$statusCode) {
            $statusCode = 307;
        }

        return $statusCode;
    }
}
