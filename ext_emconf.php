<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Redirect to page with PageTypeSuffix',
    'description' => 'Redirect urls with no page type suffix to the one with',
    'category' => 'fe',
    'author' => 'Henrik Ziegenhain',
    'author_email' => 'info@hziegenhain.de',
    'state' => 'stable',
    'clearCacheOnLoad' => true,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-10.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
